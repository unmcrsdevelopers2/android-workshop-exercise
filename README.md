# README #

This repo contain exercise Android App mocks for you to practice

### What is this repository for? ###

* HTML file that contain mocks for Android App as an exercise
* Version 1.0

### How do I get set up? ###

* Clone the repo
* Start using index.html
* hover over the component and click to check attributes (color, size, position, etc) of all the components
* Understand the mocks and try and create and app close as possible
* All the icon can be generated in the Android Studio using File > New > Image Assets.
* Have FUN

### Contribution guidelines ###

* I (Samuel Lie) will upload some new mocks from time to time as an exercise for you guys
* Feel free to ask me anything regarding this :D

### Who do I talk to? ###

* Repo owner: Samuel Lie kanasai1957@gmail.com 
* Other community or team contact